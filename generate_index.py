from string import Template

# template de lectura y archivo de salida
index_output = open("web/index.html", "w")
template_html = open("web/index_template.html", "r").read()
template_tmp  = Template(template_html)

# figuras generadas con plotly 
map_sfe = open("web/sfe_map.html", "r").read()
sol_sfe = open("web/sfe_sol.html", "r").read()
# inyectamos en el HTML
index_content = template_tmp.substitute(map = map_sfe,
                                        sol = sol_sfe,)
index_output.write(index_content)
index_output.close()